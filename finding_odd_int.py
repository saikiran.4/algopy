#O(n^2)
def find_it(seq):
    for i in set(seq):
        if seq.count(i)%2 == 1:
            return i

assert find_it([20,1,-1,2,-2,3,3,5,5,1,2,4,20,4,-1,-2,5]) == 5
assert find_it(['a', 'b', 'a']) == 'b'


#clever solution
import operator
import functools
def clever_find_it(xs):
    return functools.reduce(operator.xor, xs)


#another one without xor
def another_clever_find_it(seq):
    return functools.reduce(lambda x,y: x^y, seq)