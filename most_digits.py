"""
Find the number with the most digits.

If two numbers in the argument array have the same number of digits, return the first one in the array.
"""
def find_longest(arr):
    res = ''
    for num in arr:
        if len(str(num)) == max(len(str(num)), len(str(res))) and len(str(num)) != len(str(res)):
            res = num
        else:
            res = res
    return res

def find_longest_creative(xs):
    return max(xs, key=lambda x: len(str(x)))

assert find_longest([8, 900, 500]) == 900