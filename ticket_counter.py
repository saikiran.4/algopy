"""The new "Avengers" movie has just been released! There are a lot of people at the cinema box office standing in a huge line. Each of them has a single 100, 50 or 25 dollars bill. An "Avengers" ticket costs 25 dollars.

Vasya is currently working as a clerk. He wants to sell a ticket to every single person in this line.

Can Vasya sell a ticket to each person and give the change if he initially has no money and sells the tickets strictly in the order people follow in the line?

Return YES, if Vasya can sell a ticket to each person and give the change with the bills he has at hand at that moment. Otherwise return NO."""

def tickets(people):
    cash_box = {25:0, 50:0, 100:0}
    for i in people:

        if i == 25:
            cash_box[i] = cash_box[i] + 1
        elif i == 50 and cash_box[25] > 0:
            cash_box[i] = cash_box[i] + 1
            cash_box[25] = cash_box[25] - 1
        elif i == 100 and cash_box[50] > 0 and cash_box[25] > 0:
            cash_box[i] = cash_box[i] + 1
            cash_box[50] = cash_box[50] - 1
            cash_box[25] = cash_box[25] - 1
        elif i == 100 and cash_box[25] > 2:
            cash_box[i] = cash_box[i] + 1
            cash_box[25] = cash_box[25] - 3
        else:   return 'NO'
    
    return 'YES'
            

def clever_tickets(people):
  till = {100.0:0, 50.0:0, 25.0:0}

  for paid in people:
    till[paid] += 1
    change = paid-25.0
    
    for bill in (50,25):
      while (bill <= change and till[bill] > 0):
        till[bill] -= 1
        change -= bill

    if change != 0:
      return 'NO'
        
  return 'YES'


assert clever_tickets([25, 100]) == 'NO'
assert tickets([25, 25, 50]) == 'YES'
