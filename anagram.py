"""
An anagram is the result of rearranging the letters of a word to produce a new word (see wikipedia).

Note: anagrams are case insensitive

Complete the function to return true if the two arguments given are anagrams of theeach other; return false otherwise.

Examples

"foefet" is an anagram of "toffee"
"Buckethead" is an anagram of "DeathCubeK"
"""
# write the function is_anagram
def is_anagram(test, original):
    test = test.upper()
    original = original.upper()
    testdic = {}
    origdic = {}
    for i in test:
        testdic[i] = test.count(i)
        
    for i in original:
        origdic[i] = original.count(i)
        
    if testdic == origdic:
        return True
    else:
        return False

def is_creative_anagram(test, original):
    return sorted(test.lower()) == sorted(original.lower())

assert is_anagram('Creative', 'Reactive') == True
assert is_anagram("foefet", "toffee") == True
assert is_creative_anagram("Buckethead", "DeathCubeK") == True
assert is_anagram("Twoo", "WooT") == True
assert is_anagram("dumble", "bumble") == False
assert is_anagram("ound", "round") == False
assert is_creative_anagram("apple", "pale") == False